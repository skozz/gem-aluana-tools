require "aluanatools/version"

module Aluanatools
	
	# Hipchat notification handler   
	def self.hipchat(hipchat_key, who, message, room = "Dev Stream", notify = false,  color = 'red')
		client = HipChat::Client.new(hipchat_key)
		client[room].send("RoR " + who, message, :notify =>  notify, :color => color, :message_format => 'html')
	end

	# Get cleened extension from URL 
	def self.get_extension(url)
		require "addressable/uri"
		uri = URI.parse(URI.encode(url))
		path = uri.path
		pathname = Pathname.new(path)
		extension = pathname.extname
		return extension
	end


	# def self.web_similitary(a,b, debug = false)
	# 	begin
	# 		require 'similarity'
	# 		pageA = MetaInspector.new(a)
	# 		pageA = pageA.to_s
	# 		pageA = pageA.encode!('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '')
	# 		pageA = ActionView::Base.full_sanitizer.sanitize(pageA)

	# 		pageB = MetaInspector.new(b)
	# 		pageB = pageB.to_s
	# 		pageB = pageB.encode!('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '')
	# 		pageB = ActionView::Base.full_sanitizer.sanitize(pageB)

	# 		self.throw("pageA tiene {{#{pageA.length}} caracteres}", "info") if debug
	# 		self.throw("pageB tiene {{#{pageB.length}} caracteres}", "info") if debug

	# 		corpus = Corpus.new
	# 		doc1 = Document.new(:content => pageA)
	# 		doc2 = Document.new(:content => pageB)

	# 		[doc1, doc2].each { |doc| corpus << doc }

	# 		corpus.similar_documents(doc1).each do |doc, similarity|
	# 			if doc1.id != doc.id
	# 				@score = similarity
	# 			end
	# 		end

	# 		@score = 'cant_calculate' if !@score
				
	# 		self.hipchat(HIPCHAT_KEY, "Meathook", "Error: No se ha podido calcular la similitud entre #{a} y #{b}", "Meathook Stream", true, "red") if @score == "cant_calculate"
	# 		self.throw("La similitud entre {{#{a}}} y {{#{b}}} es #{@score}", "info") if debug

	# 		return @score
	# 	rescue => e
	# 		self.hipchat(HIPCHAT_KEY, "Meathook", "Similitary API: Excepción {{#{e}}}", "Meathook Stream", true, "red")
	# 		self.throw("#{e}", "alert")
	# 		@score = 'error' 
	# 		return @score
	# 	end
	# end





	# Parse CSV to Array
	def self.csv_to_array(url)
		require 'rubygems'
		require 'open-uri'
		require 'csv'

		begin

			@array = []
			CSV.new(open(url), {:col_sep => ', '}).each do |line|
				@array.push(line[0])
			end

			return @array					
				
		rescue => e
			self.throw("Aluanatools: Ha ocurrido un error #{__FILE__} --- #{__LINE__} - #{e}", "alert")
			return e
		end

	end

	def self.check_url_format(url, debug = false)
		url = Addressable::URI.parse(url).normalize.to_s
		self.throw("URL con parse + normalize: url") if debug
		url = clean_url(url)
		if url == "dead_link"
			return false
		elsif not /^(http:\/\/|)(https:\/\/|)(www\.|)[A-Za-z0-9_-]+\.+[A-Za-z0-9.\/%&=\?_:;-]+$/.match(url)
			return false
		else
			return true
		end
	end


	# V2 BETA
	def self.follow_redirections(url, debug = false, notification = false)
		url = Addressable::URI.parse(url).normalize.to_s
		self.throw("Follow Redirect + addressable: #{url}", "info") if debug		
		begin
			url = self.clean_url(url)
			self.throw("Follow Redirect + addre + clean: #{url}", "info") if debug
				result = Curl::Easy.perform(url) do |curl| 
					curl.headers["User-Agent"] = "Chrome/37.0.2049.0",
					curl.headers["Accept-Charset"] = "utf-8"
					curl.enable_cookies = true
					curl.verbose = false
					curl.follow_location = true
					curl.ssl_verify_peer = false
					curl.use_ssl = 3
					curl.ssl_version = 3
					curl.max_redirects = 10
					curl.timeout = 30
					# v 2.1 Futures callbacks to secure the info
					# curl.on_success 
					# curl.on_failure
					# curl.on_complete
				end

				# result.last_effective_url 
				# result.url
				self.throw("Final location: #{result.last_effective_url}", "alert") if debug

				# Aluanatools.throw("Final location: #{result.last_effective_url}", 'info')
				# Aluanatools.hipchat(HIPCHAT_KEY, "Meathook", "Final location: #{result.last_effective_url}", "Meathook Stream", false, "yellow")
				url = url
				url = self.clean_url(result.last_effective_url)
				return url 
		rescue  => e
				Aluanatools.hipchat(HIPCHAT_KEY, "Meathook", "Follow Redirect: ERROR - return false by exception > #{url}: #{e}", "Meathook Stream", true, "red") if notification
				Aluanatools.throw("ERROR: #{e}", 'alert')
				return "dead_link"
		end

	end


	def self.check_link(url, debug = false)
		require 'httparty'
		require "addressable/uri"
		url =  Addressable::URI.heuristic_parse(url, :scheme => "http")
		self.throw("URL con heuristic_parse: url","info") if debug

		url = Addressable::URI.parse(url).normalize.to_s
		self.throw("URL con heuristic_parse + parse + normalize: #{url}", "info") if debug
		url = self.follow_redirections(url)

		return false, 000 if url == "dead_link"

		begin
			resquest = HTTParty.get(url,:no_follow => false, :verify => false, :default_timeout =>30, :read_timeout =>30,
			:headers => { 
			'User-Agent' => 'Chrome/37.0.2049.0',
			'Accept-Charset' => 'utf-8'}
			)

			self.throw("#{url} - #{resquest.code}", "info") if debug


			return false, 000 if check_url_format(url) == false
			return true, resquest.code if resquest.code

		rescue => e
			self.throw("check_link: Aluanatools: Ha ocurrido un error #{__FILE__} --- #{__LINE__} - #{e}", "alert")
			return false, 000
		end
	end


	def self.clean_url(url)
		require 'uri'
		require 'open-uri' 	
			
		begin
			# Clean protocols
			protocols = [ ["http://", ""], ["https://", ""], ["HTTP://", ""], ["HTTPS://", ""] ]
			protocols.each {|pro| url.gsub!(pro[0], pro[1])}	

			# Clean the last / 
			last_char = url[url.length-1, url.length-1]
			first_char = url[0..0]
			url = url[0..-2] if last_char == "/" 

			# Clean whitespaces 
			url = url.gsub(' ', '')

			# Clean others 
			url = url.gsub("!", '%21')
			url = url.gsub("+", '%2B')
			url = url.gsub("~", '%7E')
			url = url.gsub("~", '%7E')
			url = url.gsub("(", '%28')
			url = url.gsub(")", '%29')
			url = url.gsub("@", '%40')
			url = url.gsub("*", '%2A')
			url = url.gsub("$", '%24')
			url = url.gsub('"', '%22')
			url = url.gsub('^', '%5E')
			url = url.gsub('[', '%5B')
			url = url.gsub(']', '%5D')

			# Clean anchors
			url = URI(url)
			anchor = url.fragment
			url = url.to_s
			url = url.gsub("##{anchor}", '')
		rescue => e
			return "dead_link"
		else
			return url
		end
		
	end

	def self.to_bool(string)
		string = string.to_s
		return true if string =~ (/^(true|t|yes|y|1)$/i)
		return false if string.present? || string =~ (/^(false|f|no|n|0)$/i)

		raise ArgumentError.new "invalid value: #{string}"
	end

	def self.send_to_amazon(object, name, bucket, subfolder, aws_key, aws_secret, format, debug = false)

		begin
			require 'aws-sdk'
			endpoint = "s3-eu-west-1.amazonaws.com"
			AWS::S3.new(:access_key_id => aws_key, :secret_access_key => aws_secret, :s3_endpoint => endpoint)
			bucket_name = bucket
			sub_folder = "#{subfolder}"
			file_name = "#{name}.#{format}"

			# Get an instance of the S3 interface.
			s3 = AWS::S3.new

			# Upload a file.
			key = "#{sub_folder}" + object
			s3.buckets[bucket_name].objects[key].write(:file => file_name)

			export_url = "https://#{endpoint}/#{bucket_name}/#{key}"

			self.throw("export_url: #{export_url}", "alert") if debug
			
			respond = export_url
			http_code = respond[1].to_f
				
			return true, export_url if http_code.between?(200, 299)
			return false if http_code.present? 
		rescue => e
			self.throw("send_to_amazon: Ha ocurrido un error #{__FILE__} --- #{__LINE__} - #{e}", "alert")
			return false
		end


	end



	###### BETA ######
	def self.maybe_same_site(site, type, debug = false)
		require 'domainatrix'

		term_to_check = self.follow_redirections(site, false, true)
		return false if term_to_check == "dead_link"
		term_to_check = Domainatrix.parse("#{site}").domain
		term_to_check = "#{term_to_check}"
		case type
			when "source"
				original_site = Source.where(url:  /#{term_to_check}\./, "status.type" => "success", "status.msg" => "done").last
			when "provider"
				original_site = Provider.where(url:  /#{term_to_check}\./, "status.type" => "success", "status.msg" => "done").last
		end

		# Check if exists in DB
		begin
				if original_site.present?
					begin
					
						self.throw("Term to check: #{term_to_check}", "info")  if debug
						self.throw("New url: #{site}", "warning")  if debug
						self.throw("Original url: #{original_site.url}", "warning")  if debug

						# Clean and Escape url before
						site = site.gsub("&", "%26")
						original_url = original_site.url
						original_url = original_url.gsub("&", "%26")

						# similarity = self.web_similitary(site, original_url, debug)
                                    link = "http://denver.aluana.com/text_similarity?url1=#{site}&url2=#{original_url}"
                                    response = HTTParty.get(link)
                                    similarity = JSON.parse(response)['similarity']


						self.throw("Después del get original_url: #{original_url}", "info") if debug
						self.throw("Después del get site:  #{site}", "info") if debug


						if similarity >= 98/100.to_f
							self.throw("Maybe same site: #{site} sites match", "info")  if debug
							return true
						else
							self.throw("Maybe same site: sites NOT match", "info")  if debug
							return false
						end

					rescue => e
						self.throw("Maybe same site: fatal error - #{e}", "alert")
						return false
					end
				else
					return false
				end

		rescue  => e
			self.throw("Maybe same site: fatal error - #{e}", "alert")
			return false
		end


	end # ! maybe_same_site



	def self.cast_default(default_value, default_type)
		
		if default_type=='int'
			return default_value.to_i
		
		elsif default_type=='str'
			return default_value.to_s		
	
		elsif default_type=='float'
			return default_value.to_f		
		
		elsif default_type=='array'
			return default_value		
		
		elsif default_type=='bol'
			boolean = self.to_bool(default_value)		
			return boolean
		
		elsif default_type=='obj'
			return default_value

		end
		
	end

	# current 
	def self.autoscale(heroku_key, app_name, min, max)
		heroku = Heroku::API.new(:api_key => heroku_key)
		workers = heroku.get_ps(app_name).body.select { |ps| ps["process"] =~ /worker/ }
		workers_count = workers.size
		
		if workers_count <= max
			heroku.post_ps_scale(app_name, 'worker', workers_count)
			self.hipchat(HIPCHAT_KEY, "Meathook", "Workers updated to #{workers_count}", "Meathook Stream", true, "red")
		elsif workers_count >= max
			heroku.post_ps_scale(app_name, 'worker', max)
			if max > 10
				heroku.put_formation(app_name, 'worker' => '2X')  
				self.hipchat(HIPCHAT_KEY, "Meathook", "Workers updated to #{max} - X2", "Meathook Stream", true, "red")
			else
				self.hipchat(HIPCHAT_KEY, "Meathook", "Workers updated to #{max}", "Meathook Stream", true, "red")
			end	
		elsif workers_count <= 1
			heroku.post_ps_scale(app_name, 'worker', min)
			self.hipchat(HIPCHAT_KEY, "Meathook", "Workers updated to #{max}", "Meathook Stream", true, "green")
		end
	end



	def self.throw(msg, type)
	

	#### Options
	normal = {
		:color => {
			:args       => :pale,
			:array      => :white,
			:bigdecimal => :blue,
			:class      => :yellow,
			:date       => :greenish,
			:falseclass => :red,
			:fixnum     => :blue,
			:float      => :blue,
			:hash       => :pale,
			:keyword    => :cyan,
			:method     => :purpleish,
			:nilclass   => :red,
			:rational   => :blue,
			:string     => :yellowish,
			:struct     => :pale,
			:symbol     => :cyanish,
			:time       => :greenish,
			:trueclass  => :green,
			:variable   => :cyanish
		}
	 }
	alert = {
		:color => {
			:string     => :red
		}
	 }

	info = {
		:color => {
			:string     => :cyan
		}
	 }

		case type
			when 'header'
			ap "################ #{msg} ################", normal
			when 'simple'
			ap "<< #{msg} >>", normal
			when 'alert'
			ap "!!! #{msg} !!!", alert
			when 'waiting'
			ap "#{msg} ...", normal		
			when 'ending'
			ap "... #{msg}", normal
			when 'info'
			ap "{{{{  #{msg} }}}}", info		
			when 'warning'
			ap "{{{{  #{msg} }}}}", alert

		else
			ap "<< #{msg} >>", normal
		end

	end



	def self.url_index_date(url)
		array = ["#{url}"]
		
		results = []

		array.each do |link|
			page = Nokogiri::HTML(open("http://web.archive.org/web/*/#{link}"))   
			data = page.css('#wbMeta a').map { |link| link }

			init_date = data[1].to_s 
			last_date = data[2].to_s
			reg_a = ">"
			reg_b = "</a>"

			init_date = Date.parse(init_date[/#{reg_a}(.*?)#{reg_b}/m,1]).strftime("%d/%m/%Y") 
			last_date = Date.parse(last_date[/#{reg_a}(.*?)#{reg_b}/m,1]).strftime("%d/%m/%Y") 
			ap results.push([[link], ["init_date" => init_date], ["last_date" => last_date]])
			return results
		end
	end



end
