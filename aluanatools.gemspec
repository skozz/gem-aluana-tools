# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'aluanatools/version'

Gem::Specification.new do |spec|
  spec.name          = "aluanatools"
  spec.version       = Aluanatools::VERSION
  spec.authors       = ["Angel Raul Molina"]
  spec.email         = ["skozzcraft@gmail.com"]
  spec.summary       = %q{Aluana tools}
  spec.description   = %q{Aluana tools pack}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "aws-sdk"
  spec.add_development_dependency "httparty"
  spec.add_development_dependency "heroku-api"
  spec.add_development_dependency "addressable"
  spec.add_development_dependency "rack-cors"
  spec.add_development_dependency "awesome_print"
  spec.add_development_dependency "metainspector"
  spec.add_development_dependency "domainatrix"
  spec.add_development_dependency "mechanize"
  spec.add_development_dependency "hipchat"
  spec.add_development_dependency "similarity"
  spec.add_development_dependency "curb"
end
